from django.shortcuts import render
from myWebsite.views import get_data

def index(request):
    map = get_data("pesanan_komik_pinjam")
    return render(request, "pk_pinjam.html", {'db': map})

def pk_pinjam(request):
    map = get_data("pesanan_komik_pinjam")
    return render(request, "pk_pinjam.html", {'db': map})

def pk_baca(request):
    map = get_data("pesanan_komik_baca")
    return render(request, "pk_baca.html", {'db': map})

def p_menu(request):
    map_pinjam = get_data("pesanan_menu_trpinjam")
    map_baca = get_data("pesanan_menu_trbaca")
    return render(request, "p_menu.html", {'db_pinjam': map_pinjam, 'db_baca' : map_baca})