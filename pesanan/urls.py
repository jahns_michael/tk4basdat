from django.urls import path
from .views import index, pk_pinjam, pk_baca, p_menu

urlpatterns = [
    path('', index, name='index'),
    path('pinjam/', pk_pinjam, name='pinjam'),
    path('baca/', pk_baca, name='baca'),
    path('menu/', p_menu, name='menu')
]