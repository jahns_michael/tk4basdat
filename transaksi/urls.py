from django.urls import path
from .views import index, tp_pinjam, tp_baca

urlpatterns = [
    path('', index, name='index'),
    path('pinjam/', tp_pinjam, name='pinjam'),
    path('baca/', tp_baca, name='baca')
]