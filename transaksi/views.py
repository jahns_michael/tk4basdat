from django.shortcuts import render
from myWebsite.views import get_data

def index(request):
    map = get_data("transaksi_pinjam")
    return render(request, "tp_pinjam.html", {'db': map})

def tp_pinjam(request):
    map = get_data("transaksi_pinjam")
    return render(request, "tp_pinjam.html", {'db': map})

def tp_baca(request):
    map = get_data("transaksi_baca")
    return render(request, "tp_baca.html", {'db': map})