from django.shortcuts import render
from myWebsite.views import get_data


def index(request):
    map = get_data("anggota")
    return render(request, "anggota.html", {'db': map})