from django.urls import path
from .views import index, komik, menu

urlpatterns = [
    path('', index, name='index'),
    path('komik/', komik, name='komik'),
    path('menu/', menu, name='menu')
]