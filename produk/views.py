from django.shortcuts import render
from myWebsite.views import get_data, dictfetchall
from django.db import connection

def index(request):
    map = get_data("produk")
    return render(request, "produk.html", {'db': map})

def komik(request):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to komika")
        cursor.execute("select p.id_produk, p.nama, p.jumlah, k.jumlah_available, k.seri from produk p join komik k on p.id_produk = k.id_produk")
        map = dictfetchall(cursor)
    return render(request, "komik.html", {'db': map})

def menu(request):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to komika")
        cursor.execute("select p.id_produk, p.nama, m.harga, p.jumlah from produk p join menu m on p.id_produk = m.id_produk")
        map = dictfetchall(cursor)
    return render(request, "menu.html", {'db': map})