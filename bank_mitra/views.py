from django.shortcuts import render, redirect
from myWebsite.views import get_data, dictfetchall
from django.db import connection

def index(request):
    map = get_data("bank_mitra")
    return render(request, "bank_mitra.html", {'db': map})

def add_mitra(request):
    if request.method == "POST":
        print(request.POST["mitra"])
        with connection.cursor() as cursor:
            cursor.execute("set search_path to komika")
            cursor.execute("select count(*) from bank_mitra")
            map = dictfetchall(cursor)
            count = map[0]['count'] + 1
            strcount = str(count)
            lenstr = 4-len(strcount)
            id_bank = 'B' + ('0'*lenstr) + strcount
            cursor.execute("insert into bank_mitra values (%s, %s)" , [id_bank, request.POST["mitra"]])
    return redirect("bank:index")