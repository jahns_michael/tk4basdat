from django.apps import AppConfig


class BankMitraConfig(AppConfig):
    name = 'bank_mitra'
