from django.urls import path
from .views import index, add_mitra

app_name = "bank"

urlpatterns = [
    path('', index, name='index'),
    path('add', add_mitra, name="add")
]