from django.shortcuts import render
from django.db import connection
from myWebsite.forms import LoginForm
from django.http import HttpResponseRedirect

def home(request):
    if request.method == "POST":
        form = LoginForm(data=request.POST)
        error_message = ''
        if form.is_valid() :
            data = form.cleaned_data
            no_anggota = data['nomor_anggota']
            password = data['password']
            with connection.cursor() as cursor:
                cursor.execute("set search_path to komika")
                cursor.execute("select nama, password from anggota where no_anggota = '%s'" % no_anggota)
                map = dictfetchall(cursor)
            if password == map[0]['password'] :
                request.session["nomor_anggota"] = no_anggota
                request.session["nama"] = map[0]['nama']
            else :
                error_message = 'Wrong Password'
            return render(request, "homepage.html", {'form' : form, 'message' : error_message})
    else:
        form = LoginForm()
    return render(request, "homepage.html", {'form' : form})

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def get_data(table):
    with connection.cursor() as cursor:
        cursor.execute("set search_path to komika")
        cursor.execute("select * from %s" % table)
        map = dictfetchall(cursor)
    return map

def log_out(request):
    request.session.flush()
    return HttpResponseRedirect("/")
