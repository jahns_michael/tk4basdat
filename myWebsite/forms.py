from django import forms

class LoginForm(forms.Form):
    nomor_anggota = forms.CharField(label='Nomor Anggota', max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())